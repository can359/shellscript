#!/bin/bash

# Programa para ejemplificar como capturar la informacion del usuario y validarla utilizando expresiones regulares

identificacionRegex='^[0-9]{10}$'
paisRegex='^ARG|EC|VEN$'
fechaNacimientoRegex='^19|20[0-8]{2}[1-12][1-31]$'

echo "Expresiones regulares"
read -p "Ingresar una identificacion:" identificacion
read -p "Ingresar las iniciales de un pais [ARG, EC, VEN]:" pais
read -p "Ingresar la fecha de nacimiento [yyyyMMdd]:" fechaNacimiento

# Validacion identificacion
if [[ $identificacion =~ $identificacionRegex ]]
then
    echo "Identificacion $identificacion valida"
else
    echo "Identificacion $identificacion invalida"
fi

# Validacion pais
if [[ $pais =~ $paisRegex ]]
then
    echo "Pais $pais valido"
else
    echo "Pais $pais invalido"
fi

# Validacion fecha nacimiento
if [[ $fechaNacimiento =~ $fechaNacimientoRegex ]]
then
    echo "Fecha nacimiento $fechaNacimiento valida"
else
    echo "Fecha nacimiento $fechaNacimiento invalida"
fi