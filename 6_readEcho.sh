#!/bin/bash

# Programa para ejemplificar como capturar la informacion del usuario utilizando el comando echo, read y $REPLY

option=0
backupName=""

echo "Programa utilidades postgres"
echo "Ingrese una opcion:"
read
option=$REPLY
echo "Ingrese el nombre del archivo del backup:"
read
backupName=$REPLY

echo "Option: $option, BackupName: $backupName"