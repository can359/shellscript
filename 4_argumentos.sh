#!/bin/bash

# Programa para ejemplificar el paso de argumentos

# $0            | Para obtener el nombre del script
# $1 al ${10}   | Se refiere al numero de argumentos. Si es mayor a nueve se colocan dentro de llaves
# $#            | Contador de argumentos
# $*            | Refiere a todos los argumentos

nombreCurso=$1
horarioCurso=$2

echo "El nombre del curso es: $nombreCurso dictado en el horario de $horarioCurso"
echo "El numero de parametros enviados es: $#"
echo "Los parametros enviados son: $*"