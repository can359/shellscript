#!/bin/bash

# Programa para ejemplificar el uso de la sentencia de decision if, else

notaClase=0
edad=0

echo "Ejemplo sentencia If -else"
read -n1 -p "Indique cual es su nota (1-9):" notaClase
echo ""

if (( $notaClase >= 7 ));
then
    echo "El alumno aprueba"
else
    echo "El alumno no aprueba"
fi

echo -e "\n"

read -p "indique cual es su edad:" edad
if [ $edad -le 18 ];
then
    echo "La persona no puede votar"
else
    echo "La persona puede votar"
fi